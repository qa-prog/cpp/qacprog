#include <stdio.h>
#include <stdlib.h>

typedef struct Coffee_Machine Coffee_Machine;
typedef enum Power Power;

void set_power(Coffee_Machine* cm, Power power);
void check_stock_levels(Coffee_Machine* cm);
void fill_water_ml(Coffee_Machine* cm, int  amount_ml);
void print_status(const Coffee_Machine* my_coffee_machine);

struct Coffee_Machine {
    const int WATER_CAPACITY_ML;
    enum Power{ON,OFF} power;
    int water_ml;
    int beans_g;
	int milk_ml;
	int cups_ea;
};

int main(){
    Coffee_Machine my_coffee_machine = {.WATER_CAPACITY_ML = 1000, .power = OFF};
	set_power(&my_coffee_machine, ON);
    check_stock_levels(&my_coffee_machine);
    print_status(&my_coffee_machine);
    my_coffee_machine.power = ON;
	//service_customers(&my_coffee_machine);

	char ch = 'a';
char str[] = "Hello";

printf("%s is %d bytes, %c has value %d\n"
	, str
	, sizeof(str)
	, ch
	, ch);
unsigned int max = (unsigned int) -1;
printf("Max unsigned int is: %u\n", max);

    return 0;
}

void power(Coffee_Machine* cm, Power power) {
	cm->power = power;
}

void check_stock_levels(Coffee_Machine* cm) {
	 fill_water_ml(cm, 1000);
	 //fill_beans_g(cm, 500);
	 //fill_milk_ml(cm, 200);
	 //stock_cups_ea(cm, 50);
}

void fill_water_ml(Coffee_Machine* cm, int  amount_ml) {
    cm->water_ml = amount_ml;
}

void print_status(const Coffee_Machine* cm) {
    printf("Coffee machine has %d water (max: %d)\n", cm->water_ml, cm->WATER_CAPACITY_ML );
}

