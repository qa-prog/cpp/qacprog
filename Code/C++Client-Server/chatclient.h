#pragma once

#include <string>
#include <stdexcept>
#include <memory>
#include <cstdint>
#include <errno.h>
#include <iostream>

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#define SOCK_ERROR WSAGetLastError()
#ifndef ssize_t
#define ssize_t int
#define close closesocket
#endif
#else
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define SOCK_ERROR errno
#endif

class ChatClient
{
private:
    int _sock = -1;
    unsigned short _port;
public:
    ChatClient(std::string serverName, unsigned short port);
    ChatClient(int sock);
    ~ChatClient();
    ssize_t write(std::string text);
    ssize_t write(const char* pBytes, ssize_t len);
    void read(std::string& text);
    ssize_t read(char * pBuf, ssize_t len);
};
