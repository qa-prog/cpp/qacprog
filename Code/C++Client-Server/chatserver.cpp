
#include "chatclient.h"
#include "chatserver.h"

ChatServer::ChatServer(uint16_t port)
{
    // Create the server socket using IPv4 and TCP
    _serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    // Set up the local address
    sockaddr_in serverAddress = {0};
    serverAddress.sin_family = AF_INET;
    // Set the port number
    serverAddress.sin_port = htons(port);
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    // Bind the server socket to this address
    int res = bind(_serverSocket, 
                   reinterpret_cast<struct sockaddr *>(&serverAddress),
                   sizeof(serverAddress));
    if(res == 0)
    {
        // Start listening for incoming connection
        res = listen(_serverSocket, 5);
        if(res != 0)
        {
            throw std::runtime_error("Could not listen");
        }
    }
}

ChatServer::~ChatServer()
{
    close(_serverSocket);
    _serverSocket = 0;
}

ChatClient ChatServer::WaitForClient()
{
    // Call accept to wait for a client to connect
    int fromClientSocket;
    // A structure for the client's address
    struct sockaddr_in clientAddress;
    socklen_t clientAddressSize = sizeof(clientAddress);
    fromClientSocket = accept(_serverSocket,
                              reinterpret_cast<struct sockaddr *>(&clientAddress),
                              &clientAddressSize);
    if (fromClientSocket < 0)    
    {
        throw std::runtime_error("Could not accept remote connection");
    }                      
    return ChatClient(fromClientSocket);
}
