#pragma once

#include <string>
#include <stdexcept>
#include <memory>
#include <cstdint>
#include <errno.h>

#ifdef _WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#ifndef ssize_t
#define ssize_t int
#endif
#ifndef socklen_t
#define socklen_t int
#endif
#define close closesocket
#else
#define SOCKET int  // For winsock compatibility
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

class ChatServer
{
private:
    uint16_t _port;
    SOCKET _serverSocket = 0;
public:
    ChatServer(uint16_t port);
    ~ChatServer();
    ChatClient WaitForClient();
};