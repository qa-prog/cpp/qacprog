#include <iostream>
#include <string>
#include "chatclient.h"

int main()
{
    #ifdef _WIN32
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    #endif

    const uint16_t PORT = 50001;
    const char *pServer = "127.0.0.1";
    try
    {
        ChatClient chatter(pServer, PORT);
        std::string data, reply;
        do
        {
            std::cout << "What would you like to send, bye to quit: " << std::endl;
            std::cin >> data;
            data += "\n";
            ssize_t sent = chatter.write(data);
            std::cout << "Sent " << sent << " bytes" << std::endl;
            chatter.read(reply);
            std::cout << "Reply was " << reply << std::endl;
        } while (data != "bye\n");
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
    #ifdef _WIN32
    WSACleanup();
    #endif
}
