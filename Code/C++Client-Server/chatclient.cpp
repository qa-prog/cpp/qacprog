#include "chatclient.h"

ChatClient::ChatClient(std::string serverName, unsigned short port)
{
    _port = port;
    _sock = socket(AF_INET, SOCK_STREAM, 0);
    sockaddr_in serverAddress = {0};
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(port);
    // Server address
    const char *serverIP = "127.0.0.1";
    inet_pton(AF_INET, serverName.c_str(),
              &serverAddress.sin_addr.s_addr);

    // Attempt to connect

    int res = connect(_sock,
                      reinterpret_cast<struct sockaddr *>(&serverAddress),
                      sizeof(serverAddress));
    if (res != 0)
    {
        throw std::runtime_error("Could not connect");
    }
}

ChatClient::ChatClient(int sock)
{
    _sock = sock;
}

ChatClient::~ChatClient()
{
    if (_sock)
    {
        close(_sock);
    }
}

ssize_t ChatClient::write(std::string text)
{
    ssize_t written = 0;
    if (_sock)
    {
        const char *pText = text.c_str();
        written = write(pText, text.size() + 1);
    }
    return written;
}

ssize_t ChatClient::write(const char *pBytes, ssize_t len)
{
    ssize_t written = 0;
    if (_sock)
    {
        written = send(_sock, pBytes, len, 0);
    }
    return written;
}

void ChatClient::read(std::string &text)
{
    if (_sock)
    {
        const ssize_t bufSize = 1000;
        char *pBuf = new char[bufSize];
        ssize_t got = recv(_sock, pBuf, bufSize, 0);
        if (got < 0)
        {
            throw std::runtime_error("Could not receive");
        }
        text = pBuf;
        delete[] pBuf;
        pBuf = nullptr;
    }
}

ssize_t ChatClient::read(char *pBuf, ssize_t len)
{
    ssize_t got = 0;
    if (_sock)
    {
        ssize_t got = recv(_sock, pBuf, len, 0);
        if (got < 0)
        {
            throw std::runtime_error("Could not receive");
        }
    }
    return got;
}