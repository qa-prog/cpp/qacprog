#include <iostream>

#include "chatclient.h"
#include "chatserver.h"

int main()
{
    #ifdef _WIN32
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    #endif
    const uint16_t PORT = 50001;
    try
    {
        ChatServer server(PORT);
        ChatClient client = server.WaitForClient();
        std::string msg, reply;
        do
        {
            client.read(msg);
            std::cout << "Received: " << msg << std::endl;
            client.write("Thank you\n");
        } while (msg != "bye");
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << ", error is " << SOCK_ERROR << std::endl;
    }
    #ifdef _WIN32
    WSACleanup();
    #endif
}