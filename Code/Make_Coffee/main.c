#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

const int WORKING_DAY_h = 7;
enum {MAX_NAME_STR_LEN = 50};

char name[MAX_NAME_STR_LEN] = {'\0'};
char msg[100];
int cupsOfCoffeeMade = 0;

void makeCoffee(void);
void drinkCoffee(void);
char* reportResults(char* msg);
int noOfLinesWritten(void);
char* intToStr(int number, char* result);
void print_integer_series(int start, int end);
bool there_is_work_to_do(int* work_to_go);
int getCupSize(void);
void getName(char* buffer, const int BUFF_SIZE);

int main(void)
{
    getName(name, MAX_NAME_STR_LEN);
    int work_to_go = WORKING_DAY_h;
    const int CUP_SIZE = getCupSize();
    while (there_is_work_to_do(&work_to_go) ) {
        makeCoffee();
        drinkCoffee();
        print_integer_series(1,8);
    }
    const int AMOUNT_DRUNK = CUP_SIZE * cupsOfCoffeeMade;
    printf("%s it is time to go home... after %dhrs and %d ml coffee!\n"
		, name
        , WORKING_DAY_h
        , AMOUNT_DRUNK);
	puts(reportResults(msg));
}

void makeCoffee() {
    ++cupsOfCoffeeMade;
    printf("Coffee no ");
    char msg[50];
    intToStr(cupsOfCoffeeMade, msg);
    printf(msg);
    puts(" on it's way...");
	puts("Enjoy your coffee");
}

void drinkCoffee() {
	puts("Coffee finished!");
}

char* reportResults(char* msg) {
    char numBuffer[6];
    strncpy(msg, "You have converted coffee into ", MAX_NAME_STR_LEN);
    strncat(msg, intToStr(noOfLinesWritten(),numBuffer), MAX_NAME_STR_LEN);
    strncat(msg, " lines of code", MAX_NAME_STR_LEN);
    return msg;
}

char* intToStr(int number, char* result) {
   sprintf(result, "%d", number);
   return result;
}

void print_integer_series(int start, int end) {
for (int i = start ; i < end; ++i) {
	printf("%d, ",i);
}
puts("");
}

bool there_is_work_to_do(int* work_to_go) {
	return --(*work_to_go);
}

int getCupSize(void) {
    printf("How big is your coffee cup? (ml): ");
    enum { MAX_SIZE_STR_LEN = 5 };
    char size_str[MAX_SIZE_STR_LEN ];

    fgets(size_str, MAX_SIZE_STR_LEN , stdin);

    return atoi(size_str);
}

void getName(char* buffer, const int BUFF_SIZE) {
    printf("What is your name? ");
    fgets(buffer, BUFF_SIZE, stdin);
    const int STR_LEN = strlen(buffer);
    if (STR_LEN > 0 && buffer[STR_LEN - 1] == '\n') {
        buffer[STR_LEN - 1] = '\0';
    }
}

int noOfLinesWritten() {
	return __LINE__;
}
