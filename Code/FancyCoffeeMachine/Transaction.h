#pragma once
#include <stdint.h>

typedef struct
{
	unsigned char drinkID;
	unsigned short payment;
} Transaction;

Transaction* add_transaction(Transaction* transaction_array, size_t* no_of_unsaved, int drinkID, int payment);
void save_transactions(Transaction* transaction_array, size_t* no_of_unsaved);
void print_transactions();