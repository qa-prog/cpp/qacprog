#include "UI.h"
#include <stdio.h>

void sendUserMessage(const Coffee_Machine* cm, MessageKey key, const char* sub_Message) {
	switch (key) {
	case DRNK_SEL:
		printf("Drink choices are:\n");
		for (int i = 0; i < NO_OF_DRINKS; ++i) {
			printf("%d : %s\n", i + 1, cm->drinks[i].name);
		}
		break;
	case TURN_ON:
		printf("Please turn the machine on %s:", sub_Message);
		break;
	case REFILL:
		printf("Please refill the %s hopper: ", sub_Message);
		break;
	case PAYMENT:
		printf("Please pay for your %s: ", sub_Message);
		break;
	case CHANGE:
		printf("Please take your change: %s\n", sub_Message);
		break;
	case TAKE_DRINK:
		printf("Please take your %s\n\n", sub_Message);
		break;
	case BYE:
		printf("Machine turning OFF. Goodbye\n\n");
		break;
	}	

}

int getUserSelection_int(Coffee_Machine* cm, MessageKey key, const char* sub_Message, int min, int max) {
	bool isValid;
	int choice;
	do {
		sendUserMessage(cm, key, sub_Message);
		choice = getUserInput(min, max);
		isValid = isInRange(choice, min, max);
	} while (!isValid && wantsToTryAgain());

	if (!isValid) choice = ABORT;
	return choice;
}

int getUserInput(int min, int max) {
	int selection = ABORT;
	if (max > min) {
		printf("Please make selection (%d - %d):", min, max);
	}
	else {
		printf("Please make selection (%d):", min);
	}
	scanf("%d", &selection);
	scanf("%*[^\n]");
	return selection;
}

bool wantsToTryAgain() {
	char selection = 'N';
	printf("Invalid input. Press 'Y' to try again or any key to cancel: ");
	scanf(" %c", &selection);
	scanf("%*[^\n]");
	return selection == 'Y';
}

bool isInRange(int val, int min, int max) {
	return val >= min && (max > min ? val <= max : true);
}

int optionIndex_char(char val, const char* options) {
	int nextOption = 0;
	while (options[nextOption] != '\0') {
		if (options[nextOption] == val) break;
		++nextOption;
	}
	return nextOption;
}

int getUserSelection_char(const Coffee_Machine* cm, MessageKey key, const char* sub_Message, const char* options) {
	char choice;
	int option = -1;
	do {
		sendUserMessage(cm, key, sub_Message);
		scanf(" %c", &choice);
		scanf("%*[^\n]");
		option = optionIndex_char(choice, options);
	} while (!options[option] && wantsToTryAgain());
	return option;
}
