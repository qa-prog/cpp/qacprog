#pragma once
#include "Coffee_Machine.h"
#include <stdbool.h>


void sendUserMessage(const Coffee_Machine* cm, MessageKey key, const char* sub_Message);
int getUserSelection_int(Coffee_Machine* cm, MessageKey key, const char* sub_Message, int min, int max);
int getUserSelection_char(const Coffee_Machine* cm, MessageKey key, const char* sub_Message, const char* options);
int getUserInput(int min, int max);
bool wantsToTryAgain();
bool isInRange(int val, int min, int max);
int optionIndex_char(char val, const char* options);