#pragma once

enum {DRINK_NAME_SIZE = 20};
typedef enum { WATER, MILK, SUGAR, COFFEE, TEA, CHOCOLATE, NO_OF_INGREDIENTS } Ingredient;

typedef struct 
{
	char name[DRINK_NAME_SIZE];
	int cost_p;
	int recipe[NO_OF_INGREDIENTS];
} Drink;

Drink create_drink(const char* name, int cost_p, int* ingredient_quantities_ml);