#include "Transaction.h"
#include "File_handler.h"
#include <stdlib.h>
#include <stdio.h>

static const size_t MAX_NO_OF_TRANSACTIONS = 1;

Transaction* add_transaction(Transaction* transaction_array, size_t* no_of_unsaved, int drinkID, int payment) {
	if (!transaction_array) {
		transaction_array = (Transaction*)malloc(MAX_NO_OF_TRANSACTIONS * sizeof(Transaction));
	}
	if (*no_of_unsaved >= MAX_NO_OF_TRANSACTIONS) {
		save_transactions(transaction_array, no_of_unsaved);
	}
	transaction_array[*no_of_unsaved].drinkID = drinkID;
	transaction_array[*no_of_unsaved].payment = payment;
	++(*no_of_unsaved);
	return transaction_array;
}

void save_transactions(Transaction* transaction_array, size_t* no_of_unsaved) {
	append_data("Transactions.data", transaction_array, *no_of_unsaved * sizeof(Transaction));
	*no_of_unsaved = 0;
}

void print_transactions() {
	long noOfTransactions = 0;
	Transaction* transactions = (Transaction*) read_data("Transactions.data", &noOfTransactions);
	noOfTransactions /= sizeof(Transaction);
	for (long i = 0; i < noOfTransactions; ++i) {
		printf("%d: DrinkID: %d Payment: %d\n",i, transactions[i].drinkID, transactions[i].payment);
	}
	free(transactions);
}