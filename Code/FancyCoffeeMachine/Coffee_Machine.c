#include "Coffee_Machine.h"
#include "UI.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

bool inviteTurnOn(Coffee_Machine* cm);
int stocks_OK(Coffee_Machine* cm);
void give_change(Coffee_Machine* cm, int change);
void deduct_stock(Coffee_Machine* cm, int drinkID);
void make_drink(Coffee_Machine* cm, int drinkID);

Coffee_Machine make_coffee_machine(const char* name, Hopper* hoppers, Drink* drinks) {
	Coffee_Machine cm;
	cm.state = OFF;
	cm.transactions = NULL;
	cm.no_of_unsaved_transactions = 0;
	strncpy(cm.name, name, CM_NAME_SIZE);
	for (int i = 0; i < NO_OF_INGREDIENTS; ++i) {
		cm.hoppers[i] = hoppers[i];
	}	
	for (int i = 0; i < NO_OF_DRINKS; ++i) {
		cm.drinks[i] = drinks[i];
	}
	return cm;
}

bool coffee_machine_continue(Coffee_Machine* cm) {
	switch (cm->state) {
	case OFF:
		if (inviteTurnOn(cm)) {
			cm->state = CHECK_STOCK;
		}
		break;
	case CHECK_STOCK:
		cm->selected_drinkID = stocks_OK(cm);
		if (cm->selected_drinkID == -1) {
			cm->state = REQUEST;
		}
		else {
			cm->state = REPLENISH;
		}
	break;	
	case REQUEST: 
		cm->selected_drinkID = getUserSelection_int(cm, DRNK_SEL, 0, 1, NO_OF_DRINKS);
		if (cm->selected_drinkID == ABORT) {
			cm->state = QUIT;
		}
		else {
			--cm->selected_drinkID; // ID is zero-based, cm->selected_drinkID is 1-based
			cm->state = TAKE_PAYMENT;
		}
		break;
	case REPLENISH:  
		user_refill_hopper(cm, cm->selected_drinkID);
		cm->state = CHECK_STOCK;
		break;
	case TAKE_PAYMENT:  
		cm->amount_paid_p = take_payment(cm, cm->selected_drinkID);
		if (cm->amount_paid_p != ABORT) {
			cm->state = GIVE_CHANGE;
		}
		break;
	case GIVE_CHANGE:
		if (cm->amount_paid_p > 0) {
			give_change(cm, cm->amount_paid_p);
		}
		cm->state = DEDUCT_STOCK;
		break;
	case DEDUCT_STOCK:
		deduct_stock(cm, cm->selected_drinkID);
		cm->state = MAKE_DRINK;
		break;
	case MAKE_DRINK:
		make_drink(cm, cm->selected_drinkID);
		cm->state = REQUEST;
		break;
	case QUIT:
		sendUserMessage(cm, BYE, 0);
		save_transactions(cm->transactions, &cm->no_of_unsaved_transactions);
		free(cm->transactions);
		cm->transactions = 0;
		return false;
		break;
	default:
		;
	};
	return true;
}
bool inviteTurnOn(Coffee_Machine* cm) {
	return getUserSelection_char(cm,TURN_ON,"Y/N","YyNn") < 2;
}

int stocks_OK(Coffee_Machine* cm) {
	int stockNeedsRefilling;
	for (int i = 0; i < NO_OF_DRINKS; ++i) {
		stockNeedsRefilling = drink_stock_OK(cm, i);
		if (stockNeedsRefilling >= 0) break;
	}
	return stockNeedsRefilling;
}

int drink_stock_OK(Coffee_Machine* cm, int drinkID) {
	int hopper_to_check = 0;
	for (; hopper_to_check < NO_OF_INGREDIENTS; ++hopper_to_check) {
		if (cm->hoppers[hopper_to_check].level < cm->drinks[drinkID].recipe[hopper_to_check]) {
			break;
		}
	}
	return hopper_to_check < NO_OF_INGREDIENTS ? hopper_to_check : -1;
}

bool user_refill_hopper(Coffee_Machine* cm, int hopperID) {
	int amountFilled = getUserSelection_int(cm, REFILL, cm->hoppers[hopperID].name, cm->hoppers[hopperID].capacity/4, cm->hoppers[hopperID].capacity - cm->hoppers[hopperID].level);
	if (amountFilled > 0) cm->hoppers[hopperID].level += amountFilled;
	return amountFilled > 0;
}

void pre_fill_hoppers(Coffee_Machine* cm) {
	for (int hopper_to_check = 0; hopper_to_check < NO_OF_INGREDIENTS; ++hopper_to_check) {
		cm->hoppers[hopper_to_check].level = cm->hoppers[hopper_to_check].capacity;
	}
}

int take_payment(Coffee_Machine* cm, int drinkID) {
	int payment = getUserSelection_int(cm, PAYMENT, cm->drinks[drinkID].name, cm->drinks[drinkID].cost_p, -1);
	if (payment > 0) {
		cm->transactions = add_transaction(cm->transactions, &cm->no_of_unsaved_transactions, drinkID, cm->drinks[drinkID].cost_p);
		payment -= cm->drinks[drinkID].cost_p;
	}
	return payment;
}

void give_change(Coffee_Machine* cm, int change) {
	char change_str[10];
	sprintf(change_str, "%d", change);
	sendUserMessage(cm, CHANGE, change_str);
}

void deduct_stock(Coffee_Machine* cm, int drinkID) {
	for (int hopper = 0; hopper < NO_OF_INGREDIENTS; ++hopper) {
		cm->hoppers[hopper].level -= cm->drinks[drinkID].recipe[hopper];
	}
}

void make_drink(Coffee_Machine* cm, int drinkID) {
	sendUserMessage(cm, TAKE_DRINK, cm->drinks[drinkID].name);
}
