#include "File_handler.h"
#include <stdio.h>
#include <stdlib.h>

void append_data(const char* path, void* data, int no_of_bytes) {
	FILE* my_file = fopen(path, "ab");
	if (my_file && no_of_bytes > 0) {
		fwrite(data, 1, no_of_bytes, my_file);
		fclose(my_file);
	}
}

void* read_data(const char* path, long* length) {
	FILE* my_file = fopen(path, "rb");
	void* data = NULL;
	if (my_file) {
		fpos_t start = ftell(my_file);
		fseek(my_file, 0L, SEEK_END);
		*length = ftell(my_file);
		fsetpos(my_file, &start);
		data = malloc(*length);
		fread(data, 1, *length, my_file);
		fclose(my_file);
	}
	return data;
}


