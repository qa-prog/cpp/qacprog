#pragma once
enum { HOPPER_NAME_SIZE = 20 };

typedef struct 
{
	char name[HOPPER_NAME_SIZE];
	int capacity;
	int level;
}Hopper;

Hopper make_hopper(const char* name, int capacity);
int fill_hopper(Hopper* hopper, int amount);
