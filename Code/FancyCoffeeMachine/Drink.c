#include "Drink.h"
#include <string.h>

Drink create_drink(const char* name, int cost_p, int* ingredient_quantities_ml) {
	Drink drink = { .cost_p = cost_p };
	strncpy(drink.name, name, DRINK_NAME_SIZE);
	for (int i = 0; i < NO_OF_INGREDIENTS; ++i) {
		drink.recipe[i] = ingredient_quantities_ml[i];
	}
	return drink;
}
