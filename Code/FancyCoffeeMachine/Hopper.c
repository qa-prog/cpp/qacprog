#include "Hopper.h"
#include <string.h>

Hopper make_hopper(const char* name, int capacity) {
	Hopper hopper = { .capacity = capacity, .level = 0 };
	strncpy(hopper.name, name, HOPPER_NAME_SIZE);
	return hopper;
}

int fill_hopper(Hopper* hopper, int amount) {
	if (hopper->level + amount > hopper->capacity) {
		amount = hopper->capacity - hopper->level;
	}
	hopper->level += amount;
	return  hopper->level;
}