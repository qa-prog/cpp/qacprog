#include "Coffee_Machine.h"
#include "Hopper.h"

int main() { 
	//typedef enum { WATER_HOPPER, MILK_HOPPER, SUGAR_HOPPER, COFFEE_HOPPER, TEA_HOPPER, CHOCOLATE_HOPPER} HopperType;

	Hopper hoppers[NO_OF_INGREDIENTS];
	hoppers[WATER] = make_hopper("Water", 1000);
	hoppers[MILK] = make_hopper("Milk", 500);
	hoppers[SUGAR] = make_hopper("Sugar", 500);
	hoppers[COFFEE] = make_hopper("Coffee Beans", 500);
	hoppers[TEA] = make_hopper("Tea", 500);
	hoppers[CHOCOLATE] = make_hopper("Chocolate", 500);

	int americano_recipe[NO_OF_INGREDIENTS] = { 100,0,0,20 };
	int espresso_recipe[NO_OF_INGREDIENTS] = { 50,0,0,20 };
	int tea_recipe[NO_OF_INGREDIENTS] = { 100,20,0,0,10 };
	int choc_recipe[NO_OF_INGREDIENTS] = { 50,50,10,0, 0, 50 };

	Drink drinks[] = {
		  create_drink("Americano", 250, americano_recipe)
		, create_drink("Espresso", 240, espresso_recipe)
		, create_drink("Tea", 200, tea_recipe)
		, create_drink("Hot Chocolate", 300, choc_recipe)
	};
	Coffee_Machine cm = make_coffee_machine("Stephen's Machine", hoppers, drinks);
	pre_fill_hoppers(&cm);
	while (coffee_machine_continue(&cm));
	print_transactions();
}