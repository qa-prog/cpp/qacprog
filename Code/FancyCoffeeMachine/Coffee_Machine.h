#pragma once
#include "Hopper.h"
#include "Drink.h"
#include "Transaction.h"
#include <stdbool.h>

enum { CM_NAME_SIZE = 20, NO_OF_DRINKS = 4 };
enum { USER_INPUT = -2, ABORT };
typedef enum { TURN_ON, DRNK_SEL, REFILL, PAYMENT, CHANGE, TAKE_DRINK, BYE }MessageKey;

typedef enum {
	OFF
	, CHECK_STOCK
	, REQUEST
	, REPLENISH
	, TAKE_PAYMENT
	, GIVE_CHANGE
	, DEDUCT_STOCK
	, MAKE_DRINK
	, RECORD_TRANSACTION
	, QUIT
} CM_State;

typedef struct
{
	char name[CM_NAME_SIZE];
	Hopper hoppers[NO_OF_INGREDIENTS];
	Drink drinks[NO_OF_DRINKS];
	CM_State state;
	int selected_drinkID;
	int amount_paid_p;
	Transaction* transactions;
	size_t no_of_unsaved_transactions;
} Coffee_Machine;

Coffee_Machine make_coffee_machine(const char* name, Hopper* hoppers, Drink* drinks);
bool coffee_machine_continue(Coffee_Machine* cm);
bool user_refill_hopper(Coffee_Machine* cm, int hopperID);
int drink_stock_OK(Coffee_Machine* cm, int drinkID);
void pre_fill_hoppers(Coffee_Machine* cm);
int take_payment(Coffee_Machine* cm, int drinkID);
