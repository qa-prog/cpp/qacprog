@echo off

if not exist "%1" (
    mkdir "%1"
    echo Folder created: "%1"
) else (
    echo Folder already exists: "%1"
)

:: Running inline PowerShell script

for /f "delims=" %%i in ('powershell -Command "$roots = @('C:\Program Files (x86)\Microsoft Visual Studio', 'C:\Program Files\Microsoft Visual Studio'); foreach ($root in $roots) { $filePath = Get-ChildItem -Path $root -Recurse -Filter 'VsDevCmd.bat' | Select-Object -ExpandProperty FullName; if ($filePath) { Write-Output $filePath; break; } }"') do set "vs_cmd_path=%%i"

call "%vs_cmd_path%"